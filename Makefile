all: v4l-open media-info
#	make test-open
#	make test-media-info
v4l-open: v4l-open.c
	gcc v4l-open.c -o v4l-open
media-info: media-info.c
	gcc media-info.c -o media-info
test-open: v4l-open
	./unbind-bind.sh ./v4l-open /dev/v4l-subdev
test-media-info: media-info
	./unbind-bind.sh ./media-info /dev/media
clean:
	rm v4l-open media-info
	
