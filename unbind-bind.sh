#!/bin/bash

device=vimc.0
driver=vimc

while [ 1 ]; do
	for ((j=0;j<=5;j++)); do
		${1} ${2}${j} &
	done
	echo -n ${device} >/sys/bus/platform/drivers/${driver}/unbind &
	echo -n ${device} >/sys/bus/platform/drivers/${driver}/bind &
done

