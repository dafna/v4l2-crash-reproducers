# This script tests vimc for unbinding the device while it streams.
# enable CONFIG_KASAN to get a report of bugs that it might catch.

modprobe vimc
media-ctl -d platform:vimc -V '"Sensor A":0[fmt:SBGGR8_1X8/640x480],"Debayer A":0[fmt:SBGGR8_1X8/640x480]'

v4l2-ctl -d2 -v width=1920,height=1440
v4l2-ctl -d0 -v pixelformat=BA81
v4l2-ctl --stream-mmap --stream-count=1000 -d /dev/video2 &
sleep 0.5
echo -n vimc.0 >/sys/bus/platform/drivers/vimc/unbind
sleep 0.1
echo -n vimc.0 >/sys/bus/platform/drivers/vimc/bind
